package edu.sjsu.android.mortgagecalculator;
//@author Arlan Prado
public class MortgageCalculator {
    public static float calculateMortgageWithInterest(float borrowed, float yearlyInterest, int loanTerm, float taxes){
        float denom = 1 - (float)Math.pow(1 + (yearlyInterest / 1200), -(loanTerm * 12));
        float temp = (yearlyInterest / 1200) / denom;
        temp = borrowed * temp;
        temp = temp + taxes;
        return temp;
    }
    public static float calculateMortgageNoInterest(float borrowed, int loanTerm, float taxes){
        return (borrowed/(loanTerm * 12)) + taxes;
    }
}
