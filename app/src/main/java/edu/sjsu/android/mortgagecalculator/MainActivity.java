package edu.sjsu.android.mortgagecalculator;
//@author Arlan Prado
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class MainActivity extends AppCompatActivity {

    private EditText amtBorrowedTxt;
    private TextView interestText;
    private SeekBar interestBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        amtBorrowedTxt = (EditText) findViewById(R.id.editTextAmountBorrowed);
        interestText = findViewById(R.id.interestText);
        interestBar = findViewById(R.id.interestBar);
        interestBar.setProgress(10);
        interestText.setText(String.valueOf(interestBar.getProgress()) + "%");
        interestBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                interestText.setText(String.valueOf(interestBar.getProgress()) + "%");
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                //interestText.setText(String.valueOf(interestBar.getProgress()) + "%");
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                //interestText.setText(String.valueOf(interestBar.getProgress()) + "%");
            }
        });

    }
    public void onClick(View view){
        switch(view.getId()){
            case R.id.calculateButton:
                float amtBorrowed = 0;
                TextView mortgageText = (TextView) findViewById(R.id.monthlyPaymentText);
                try{
                    amtBorrowed = Float.valueOf(amtBorrowedTxt.getText().toString());
                }catch(NumberFormatException e){
                    Toast.makeText(this, "Please enter a valid number", Toast.LENGTH_LONG).show();
                    return;
                }

                RadioButton years15Button = (RadioButton) findViewById(R.id.loanRadioButton1);
                RadioButton years20Button = (RadioButton) findViewById(R.id.loanRadioButton2);
                //RadioButton years30Button = (RadioButton) findViewById(R.id.loanRadioButton3);
                CheckBox taxCheckBox = (CheckBox) findViewById(R.id.taxCheckBox);

//                if(amtBorrowedTxt.getText().length() == 0){ //could try using try catch
//                    Toast.makeText(this, "Please enter a valid number", Toast.LENGTH_LONG).show();
//                    return;
//                }

                amtBorrowed = Float.parseFloat(amtBorrowedTxt.getText().toString());
                float yearlyInterest = Float.valueOf(interestBar.getProgress());
                int loanTerm;
                float taxes = 0;
                if(years15Button.isChecked())
                    loanTerm = 15;
                else if(years20Button.isChecked())
                    loanTerm = 20;
                else
                    loanTerm = 30;

                if(taxCheckBox.isChecked())
                    taxes = (float)0.10 * amtBorrowed;

                if(yearlyInterest > 0) {
                    BigDecimal bd = new BigDecimal(MortgageCalculator.calculateMortgageWithInterest(amtBorrowed, yearlyInterest, loanTerm, taxes)).setScale(2, RoundingMode.HALF_UP);
                    mortgageText.setText("$" + bd);
                }else{
                    BigDecimal bd = new BigDecimal(MortgageCalculator.calculateMortgageNoInterest(amtBorrowed, loanTerm, taxes)).setScale(2, RoundingMode.HALF_UP);
                    mortgageText.setText("$" + bd);
                }
                return;

                //mortgageText.setText("Test Failed");
        }

    }
}